from django.core.management.base import BaseCommand, CommandError
from games.models import Word


class Command(BaseCommand):
    help = "Loads a word file into the DB"

    def add_arguments(self, parser):
        parser.add_argument("word_list", nargs=1)

    def handle(self, *args, **options):
        word_file = open(options["word_list"][0], "r")
        while True:
            word_line = word_file.readline()

            if not word_line:
                break

            word = Word()
            word.spelling = word_line.strip()
            word.source = "words"
            word.save()

            self.stdout.write(f"{word.spelling} added")


