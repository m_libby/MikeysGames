from django.db import models
from random import randint
from .word import Word

# Create your models here.
class PathwordsGame(models.Model):
    size = models.IntegerField(default=4)
    letters = models.CharField(max_length=256)

    dice = [
        "aaeegn",
        "abbjoo",
        "achops",
        "affkps",
        "aoottw",
        "cimotu",
        "deilrx",
        "delrvy",
        "distty",
        "eeghnw",
        "eeinsu",
        "ehrtvw",
        "eiosst",
        "elrtty",
        "himnuq",
        "hlnnrz",
    ]

    def roll_dice(self):
        letters = []
        for die in self.dice:
            face = randint(0, 5)
            letter = die[face]
            letters.append(letter)
        self.letters = "".join(letters)

    def get_grid(self):
        grid = []
        for y in range(self.size):
            row = []
            for x in range(self.size):
                letter = self.letters[x + y * self.size]
                if letter == "q":
                    letter = "Qu"
                else:
                    letter = letter.upper()
                row.append(letter)
            grid.append(row)
        return grid

    def score(self, words_submitted):
        words = list(set(words_submitted.split()))
        valid = []
        invalid = []
        total = 0

        for word in words:
            valid_word = Word.objects.filter(spelling=word).first()
            if valid_word:
                score = self.score_word(word)
                valid.append({"word": word, "score": score})
                total += score
            else:
                invalid.append(word)

        return {"total": total, "valid": valid, "invalid": invalid}

    def score_word(self, word):
        length = len(word)
        if length < 5:
            return 1
        if length == 5:
            return 2
        if length == 6:
            return 3
        if length == 7:
            return 5
        if length > 7:
            return 6
