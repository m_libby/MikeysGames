from django.db import models
from random import randint

# Create your models here.
class Word(models.Model):
    spelling = models.CharField(primary_key=True, max_length=64, unique=True)
    source = models.CharField(max_length=64)