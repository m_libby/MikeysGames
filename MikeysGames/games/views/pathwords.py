from django.shortcuts import get_object_or_404, redirect, render
from games.models import PathwordsGame


def pathwords_start(request):
    if request.method == "GET":
        return render(request, "pathwords/start.html", {"title": "Pathwords"})
    elif request.method == "POST":
        pw = PathwordsGame()
        pw.roll_dice()
        pw.save()
        return redirect(f"/pathwords/play/{pw.id}")


def pathwords_play(request, game_id):
    game = get_object_or_404(PathwordsGame, pk=game_id)
    if request.method == "GET":
        return render(
            request,
            "pathwords/play.html",
            {"title": "Pathwords", "game_id": game.id, "rows": game.get_grid()},
        )
    elif request.method == "POST":
        words = request.POST["words"]
        results = game.score(words)
        return render(
            request,
            "pathwords/score.html",
            {"title": "Pathwords", "score": results["total"], "valid": results["valid"], "invalid": results["invalid"]},
        )
