require "rqrcode"

class MikeysGames < Sinatra::Base
  def qr_code(url)
    qrcode = RQRCode::QRCode.new(url)
    qrcode.as_svg(
      offset: 0,
      color: "000",
      shape_rendering: "crispEdges",
      module_size: 6,
      standalone: true,
    )
  end

  get "/" do
    if get_current_game.nil?
      erb_layout :"home/play"
    else
      redirect "/pathwords/play"
    end
  end

  get "/serve" do
    addr = Socket.ip_address_list.detect { |addr| addr.ipv4_private? }
    port = request.env["SERVER_PORT"]
    @server = "#{addr.ip_address}:#{port}"
    @qr_code = qr_code("http://#{@server}/")
    if get_current_game.nil?
      erb_layout :"home/serve"
    else
      redirect "/pathwords/serve"
    end
  end

  get "/util" do
    erb_layout :"home/util"
  end

  get "/touch" do
    erb_layout :"home/touch"
  end
end
