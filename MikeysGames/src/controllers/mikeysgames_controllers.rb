require_relative "home"
require_relative "pathwords"

class MikeysGames < Sinatra::Base

@@current_game = nil

def get_current_game(game_name = "")
  if @@current_game.nil?
    if game_name != "Pathwords"
      return nil
    else
      return @@current_game = Pathwords.instance
    end
  else
    return @@current_game
  end
end

end